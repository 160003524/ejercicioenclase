factorial(0,1).
factorial(X,N) :-
    X>0,X1 is X-1,
    factorial(X1,N1),
    N is X * N1.

fibona(0,0).
fibona(1,1).
fibona(X,N):-
    X1 is X-1,X2 is X1-1,
    fibona(X1,N1),
    fibona(X2,N2),
    N is N1 + N2.

horoscopo(aries,21,3,21,4).
horoscopo(tauro,21,4,21,5).
horoscopo(geminis,21,5,21,6).
horoscopo(cancer,21,6,21,7).
horoscopo(leo,21,7,21,8).
horoscopo(virgo,21,8,21,9).
horoscopo(libra,21,9,21,10).
horoscopo(escorpio,21,10,21,11).
horoscopo(sagitario,21,11,21,12).
horoscopo(capricornio,21,12,21,1).
horoscopo(acuario,21,1,21,2).
horoscopo(piscis,21,2,21,3). 
signo(Signo,Dia,Mes):-
    horoscopo(Signo,DIA1,MES1,DIA2,MES2),
    (   (   Mes=MES1,Dia>=DIA1);(   Mes=MES2,Dia=<DIA2)).

minimo(N,N,N).
minimo(N,Y,D):-
    N<Y,Y1 is Y-N,
    minimo(N,Y1,D).
minimo(N,Y,D):-
    Y<N,
    minimo(Y,N,D).

potencia(H,0,1).
potencia(H,P,R):-
    P>0, P1 is P-1,
    potencia(H,P1,R1),
    R is H * R1.

